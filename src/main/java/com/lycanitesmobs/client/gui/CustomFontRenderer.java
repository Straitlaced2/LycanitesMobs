package com.lycanitesmobs.client.gui;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.fonts.Font;
import net.minecraft.client.renderer.texture.TextureManager;

public class CustomFontRenderer extends FontRenderer {

	public CustomFontRenderer(TextureManager textureManager, Font font) {
		super(textureManager, font);
	}
}
